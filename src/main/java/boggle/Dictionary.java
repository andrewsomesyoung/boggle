//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

import java.io.InputStream;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class Dictionary
{
    private SortedSet<String> words = new TreeSet<>();

    public Dictionary()
    {
        this(Dictionary.class.getResourceAsStream("words.txt"));
    }

    public Dictionary(InputStream inputStream)
    {
        Scanner scanner = new Scanner(inputStream);
        while (scanner.hasNextLine())
        {
            words.add(scanner.nextLine().toUpperCase());
        }
    }

    public boolean containsPrefix(String prefix)
    {
        return !words.tailSet(prefix).headSet(prefix+"[").isEmpty();
    }

    public boolean containsWord(String word)
    {
        return words.contains(word);
    }
}
