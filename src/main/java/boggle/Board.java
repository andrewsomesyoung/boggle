//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Board
{
    private List<List<String>> rows;
    private List<Position> positions = new ArrayList<>();

    public Board(List<List<String>> rows)
    {
        this.rows = rows;

        for (int row = 0;row < rows.size();row++)
        {
            for (int col = 0;col < rows.get(row).size();col++)
            {
                positions.add(new Position(row, col));
            }
        }
    }

    public int getWidth()
    {
        return rows.get(0).size();
    }

    public int getHeight()
    {
        return rows.size();
    }

    public Iterator<Position> positionsIt()
    {
        return positions.iterator();
    }

    public String getCharacter(Position position)
    {
        return rows.get(position.getRow()).get(position.getCol());
    }

    public List<Position> getAdjacentPositions(Position position)
    {
        List<Position> results = new ArrayList<>();

        int row;

        // Previous row
        if (position.getRow() > 0)
        {
            row = position.getRow() - 1;

            // Previous column
            if (position.getCol() > 0)
            {
                results.add(new Position(row, position.getCol() - 1));
            }

            results.add(new Position(row, position.getCol()));

            // Next column
            if (position.getCol() + 1 < getWidth())
            {
                results.add(new Position(row, position.getCol() + 1));
            }
        }

        // Current row
        row = position.getRow();

        // Previous column
        if (position.getCol() > 0)
        {
            results.add(new Position(row, position.getCol() - 1));
        }

        // Next column
        if (position.getCol() + 1 < getWidth())
        {
            results.add(new Position(row, position.getCol() + 1));
        }

        // Next row
        if (position.getRow() + 1 < getHeight())
        {
            row = position.getRow() + 1;

            // Previous column
            if (position.getCol() > 0)
            {
                results.add(new Position(row, position.getCol() - 1));
            }

            results.add(new Position(row, position.getCol()));

            // Next column
            if (position.getCol() + 1 < getWidth())
            {
                results.add(new Position(row, position.getCol() + 1));
            }
        }

        return results;
    }
}
