//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Solver
{
    private Dictionary dictionary = new Dictionary();

    public List<String> solve(Board board)
    {
        List<String> results = new ArrayList<>();

        Iterator<Position> it = board.positionsIt();
        while (it.hasNext())
        {
            results.addAll(wordsFrom(board, it.next(), "", new ArrayList<>()));
        }

        return results;
    }

    private List<String> wordsFrom(Board board, Position position, String prefix, List<Position> used)
    {
        List<String> results = new ArrayList<>();

        prefix = prefix + board.getCharacter(position);

        // If prefix is not in dictionary, bail
        if (!dictionary.containsPrefix(prefix))
        {
            return results;
        }

        // Mark position as used
        used.add(position);

        // If prefix is entire word, add it to results
        if (dictionary.containsWord(prefix))
        {
            results.add(prefix);
        }

        // Process remaining adjacent characters recursively
        List<Position> adjacencies = board.getAdjacentPositions(position);
        adjacencies.removeAll(used);

        for (Position next : adjacencies)
        {
            results.addAll(wordsFrom(board, next, prefix, new ArrayList<>(used)));
        }

        return results;
    }
}
