//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BoardParser
{
    public Board parse(InputStream inputStream)
    {
        List<List<String>> rows = new ArrayList<>();

        Scanner rowScanner = new Scanner(inputStream);
        while (rowScanner.hasNextLine())
        {
            List<String> row = new ArrayList<>();

            Scanner colScanner = new Scanner(rowScanner.nextLine());
            while (colScanner.hasNext())
            {
                row.add(colScanner.next());
            }

            rows.add(row);
        }

        return new Board(rows);
    }
}
