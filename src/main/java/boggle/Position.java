//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

public class Position
{
    private int row;
    private int col;

    public Position(int row, int col)
    {
        this.row = row;
        this.col = col;
    }

    public int getRow()
    {
        return row;
    }

    public int getCol()
    {
        return col;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Position))
        {
            return false;
        }

        Position position = (Position)obj;

        return row == position.row && col == position.col;
    }

    @Override
    public int hashCode()
    {
        int result = 17;

        result = 37*result + row;
        result = 37*result + col;

        return result;
    }
}
