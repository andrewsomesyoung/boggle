//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BoardTest
{
    @Test
    public void testPositionsIt()
    {
        // Given - Sample Board 1
        //
        //  R P F I
        //  E T H I
        //  U S S E
        //  A T E A
        //

        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream("sampleBoard1.txt"));

        // Then - the first character should be R

        assertEquals("R", board.getCharacter(board.positionsIt().next()));
    }

    @Test
    public void testAdjacentPositions()
    {
        List<Position> adjacencies = null;

        // Given - Sample Board 1
        //
        //  R P F I
        //  E T H I
        //  U S S E
        //  A T E A
        //

        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream("sampleBoard1.txt"));

        // Then - the positions adjacent to (0,0) should be (0,1), (1,1), (1,0)

        adjacencies = board.getAdjacentPositions(new Position(0, 0));
        assertTrue(adjacencies.contains(new Position(0, 1)));
        assertTrue(adjacencies.contains(new Position(1, 1)));
        assertTrue(adjacencies.contains(new Position(1, 0)));
        assertEquals(3, adjacencies.size());

        // And - the positions adjacent to (1,1) should be (0,0), (0,1), (0,2), (1,0), (1,2), (2,0), (2,1), (2,2)

        adjacencies = board.getAdjacentPositions(new Position(1, 1));
        assertTrue(adjacencies.contains(new Position(0, 0)));
        assertTrue(adjacencies.contains(new Position(0, 1)));
        assertTrue(adjacencies.contains(new Position(0, 2)));
        assertTrue(adjacencies.contains(new Position(1, 0)));
        assertTrue(adjacencies.contains(new Position(1, 2)));
        assertTrue(adjacencies.contains(new Position(2, 0)));
        assertTrue(adjacencies.contains(new Position(2, 1)));
        assertTrue(adjacencies.contains(new Position(2, 2)));
        assertEquals(8, adjacencies.size());
    }

}
