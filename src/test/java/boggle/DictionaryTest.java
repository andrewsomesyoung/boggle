//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DictionaryTest
{
    @Test
    public void testContains()
    {
        Dictionary dictionary = new Dictionary();
        assertTrue(dictionary.containsPrefix("HA"));
        assertFalse(dictionary.containsPrefix("ABCDEFG"));
    }
}
