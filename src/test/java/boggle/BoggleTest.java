//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class BoggleTest
{
    @Test
    public void testSampleBoard1()
    {
        // Given - Sample Board 1
        //
        //  R P F I
        //  E T H I
        //  U S S E
        //  A T E A
        //

        String sampleFile = "sampleBoard1.txt";
        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream(sampleFile));

        // When - We solve it

        List<String> words = new Solver().solve(board);

        // Then - The solutions should contain PEST, SEE, TEST, HISS, TASTE

        assertTrue(words.contains("PEST"));
        assertTrue(words.contains("SEE"));
        assertTrue(words.contains("TEST"));
        assertTrue(words.contains("HISS"));
        assertTrue(words.contains("TASTE"));

        System.out.println("Found " + words.size() + " words in " + sampleFile);
    }

    @Test
    public void testSampleBoard2()
    {
        // Given - Sample Board 2
        //
        //  B B L Y
        //  E N S O
        //  N G E A
        //  A S L E
        //

        String sampleFile = "sampleBoard2.txt";
        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream(sampleFile));

        // When - We solve it

        List<String> words = new Solver().solve(board);

        // Then - The solutions should contain AGE, LOSE, LEG

        assertTrue(words.contains("AGE"));
        assertTrue(words.contains("LOSE"));
        assertTrue(words.contains("LEG"));

        System.out.println("Found " + words.size() + " words in " + sampleFile);
    }

    @Test
    public void testSampleBoard3()
    {
        // Given - Sample Board 3
        //
        //  A E M U
        //  X I E L
        //  A E L U
        //  S R B S
        //

        String sampleFile = "sampleBoard3.txt";
        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream(sampleFile));

        // When - We solve it

        List<String> words = new Solver().solve(board);

        // Then - The solutions should contain AXE, SEE, SEA, ME

        assertTrue(words.contains("AXE"));
        assertTrue(words.contains("SEE"));
        assertTrue(words.contains("SEA"));
        assertTrue(words.contains("ME"));

        System.out.println("Found " + words.size() + " words in " + sampleFile);
    }

    @Test
    public void testSampleBoard4()
    {
        // Given - Sample Board 4
        //
        //  B O B O
        //  O B O B
        //  B O B O
        //  O B O B
        //

        String sampleFile = "sampleBoard4.txt";
        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream(sampleFile));

        // When - We solve it

        List<String> words = new Solver().solve(board);

        // Then - The solutions should contain BOB

        assertTrue(words.contains("BOB"));

        System.out.println("Found " + words.size() + " words in " + sampleFile);
    }

    @Test
    public void testPerformance1()
    {
        // Given - Sample Board 1
        //
        //  R P F I
        //  E T H I
        //  U S S E
        //  A T E A
        //

        String sampleFile = "sampleBoard1.txt";
        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream(sampleFile));

        // When - we measure average solver performance

        Solver solver = new Solver();

        long startTime = System.currentTimeMillis();

        solver.solve(board);

        long msPerSolve = (System.currentTimeMillis() - startTime);

        System.out.println("First solve time " + msPerSolve + " ms");

        startTime = System.currentTimeMillis();

        for (int i = 0;i < 500;i++)
        {
            solver.solve(board);
        }

        msPerSolve = (System.currentTimeMillis() - startTime) / 500;

        System.out.println("Average solve time " + msPerSolve + " ms");
    }

    @Test
    public void testPerformance4()
    {
        // Given - Sample Board 4
        //
        //  B O B O
        //  O B O B
        //  B O B O
        //  O B O B
        //

        String sampleFile = "sampleBoard4.txt";
        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream(sampleFile));

        // When - we measure average solver performance

        Solver solver = new Solver();

        long startTime = System.currentTimeMillis();

        solver.solve(board);

        long msPerSolve = (System.currentTimeMillis() - startTime);

        System.out.println("First solve time " + msPerSolve + " ms");

        startTime = System.currentTimeMillis();

        for (int i = 0;i < 500;i++)
        {
            solver.solve(board);
        }

        msPerSolve = (System.currentTimeMillis() - startTime) / 500;

        System.out.println("Average solve time " + msPerSolve + " ms");
    }

    @Test
    public void testPerformance5()
    {
        // Given - Sample Board 5
        //
        //  B O B O B O B O B O
        //  O B O B O B O B O B
        //  B O B O B O B O B O
        //  O B O B O B O B O B
        //  B O B O B O B O B O
        //  O B O B O B O B O B
        //  B O B O B O B O B O
        //  O B O B O B O B O B
        //  B O B O B O B O B O
        //  O B O B O B O B O B
        //

        String sampleFile = "sampleBoard5.txt";
        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream(sampleFile));

        // When - we measure average solver performance

        Solver solver = new Solver();

        long startTime = System.currentTimeMillis();

        solver.solve(board);

        long msPerSolve = (System.currentTimeMillis() - startTime);

        System.out.println("First solve time " + msPerSolve + " ms");

        startTime = System.currentTimeMillis();

        for (int i = 0;i < 500;i++)
        {
            solver.solve(board);
        }

        msPerSolve = (System.currentTimeMillis() - startTime) / 500;

        System.out.println("Average solve time " + msPerSolve + " ms");
    }
}
