//
// Moscow - Copyright 2018 Andrew Young (joven@alum.wpi.edu)
//
package boggle;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoardParserTest
{
    @Test
    public void testSampleInput1()
    {
        // Given - Sample Board 1
        //
        //  R P F I
        //  E T H I
        //  U S S E
        //  A T E A
        //

        BoardParser parser = new BoardParser();
        Board board = parser.parse(getClass().getResourceAsStream("sampleBoard1.txt"));

        // Then - the width and height should both be 4

        assertEquals(4, board.getWidth());
        assertEquals(4, board.getHeight());
    }
}
